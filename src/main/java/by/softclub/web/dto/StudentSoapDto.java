package by.softclub.web.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "StudentSoapDto", namespace = "http://ws.tim.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StudentSoapDto", namespace = "http://ws.tim.com/")
public class StudentSoapDto {
    private int id;
    private String name;

    public String getName() {
        return name;
    }
}
