package by.softclub.web.service;

import by.softclub.web.dto.StudentSoapDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class StudentWebServiceImpl  {

    @WebMethod()
    public String hello(@WebParam(name = "name") String name) {
        return "Hello " + name;
    }

    @WebMethod()
    public String helloStudnetName(@WebParam(name = "dto") StudentSoapDto name) {
        return "Hello " + name.getName();
    }
}
