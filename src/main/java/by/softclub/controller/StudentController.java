package by.softclub.controller;

import by.softclub.service.StudentService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/student")
@Produces(MediaType.APPLICATION_JSON)
public class StudentController {

    @Inject
    private StudentService studentService;

    @GET
    @Path("/list")
    public Response getStudents() {

        Response response;

        try {
            response = Response.ok(studentService.getStudentListJson()).build();
        } catch (Exception e) {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                    e.getMessage()).build();
        }

        return response;

    }

}
