package by.softclub.repository;

import by.softclub.model.Student;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class StudentRepository {

    @PersistenceContext
    private EntityManager em;

    public List<Student> getList() {
        return em.createQuery("select st from Student st", Student.class)
                .getResultList();
    }

    public List<Student> getStudentByName(String name) {
        return em.createQuery("select st from Student st where st.name = :name", Student.class)
                .setParameter("name", name)
                .getResultList();
    }

    
}
