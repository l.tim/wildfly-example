package by.softclub.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "STUDENT")
public class Student {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="student_seq")
    @SequenceGenerator(
            name="student_seq",
            sequenceName="student_id_seq",
            allocationSize = 1,
            initialValue = 30
    )
    private long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "AGE")
    private int age;
}
