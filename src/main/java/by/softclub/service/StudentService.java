package by.softclub.service;

import by.softclub.model.Student;

import javax.json.JsonArray;
import java.util.List;

public interface StudentService {

    List<Student> getStudents();

    JsonArray getStudentListJson();
}
