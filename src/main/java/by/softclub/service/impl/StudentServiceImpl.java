package by.softclub.service.impl;

import by.softclub.model.Student;
import by.softclub.repository.StudentRepository;
import by.softclub.service.StudentService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.stream.JsonCollectors;
import java.util.List;

@Stateless
public class StudentServiceImpl implements StudentService {

    @Inject
    private StudentRepository studentRepository;

    @Override
    public List<Student> getStudents() {
        return studentRepository.getList();
    }

    @Override
    public JsonArray getStudentListJson() {
        return getStudents()
                .stream()
                .map(this::buildStudentJson)
                .collect(JsonCollectors.toJsonArray());
    }

    private JsonObject buildStudentJson(Student student) {
        return Json.createObjectBuilder()
                .add("id", student.getId())
                .add("fullName", student.getName())
                .build();
    }
}
